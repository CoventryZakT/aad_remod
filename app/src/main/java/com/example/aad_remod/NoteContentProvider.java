package com.example.aad_remod;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.provider.ContactsContract;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class NoteContentProvider extends ContentProvider {

    public static final String AUTHORITY =  "com.example.aad_remod.notecontentprovider";

    private static final String BASE_PATH_NOTE = "notes";

    public static final Uri CONTENT_URI = Uri.parse("content://"+AUTHORITY+"/"+BASE_PATH_NOTE);

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    //arbitrary numerical values to identify requested operation
    private static final int NOTES = 100; // Full data
    private static final int NOTES_ID = 101; //Provides single record

    static {
        uriMatcher.addURI(AUTHORITY, BASE_PATH_NOTE, NOTES);
        uriMatcher.addURI(AUTHORITY, BASE_PATH_NOTE+"/#", NOTES_ID);
    }

    private SQLiteDatabase database;
    private DBHelper dbHelper;

    @Override
    public boolean onCreate(){
        dbHelper = new DBHelper(getContext());
        return false;

    }


    @Override
    public Cursor query( Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        int type = URI_MATCHER.match(uri);
        switch (type){

            case NOTES_ID:
                break;

            case NOTES:
                queryBuilder.appendWhere(DBHelper.NOTE_ID + "=" + uri.getLastPathSegment());
                break;

            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = queryBuilder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(),uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri){
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues){

        int type = URI_MATCHER.match(uri);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Long id;
        switch (type){
            case NOTES:
                id = database.insert(DBHelper.TABLE_NOTES, null, contentValues);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri,null);
        return Uri.parse(BASE_PATH_NOTE+"/"+id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs){

        int type = URI_MATCHER.match(uri);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        int affectedRows;

        switch (type) {
            case NOTES:
                affectedRows = database.delete(DBHelper.TABLE_NOTES, selection, selectionArgs);
                break;

            case NOTES_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)){
                    affectedRows = database.delete(DBHelper.TABLE_NOTES, DBHelper.NOTE_ID+"="+id,null);
                }
                else {
                    affectedRows = database.delete(DBHelper.TABLE_NOTES, DBHelper.NOTE_ID+ "=" +" and " + selection, selectionArgs);
                }
                break;

                default:
                    throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri,null);

        return affectedRows;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs){

        int type = URI_MATCHER.match(uri);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        int affectedRows;
        switch (type){

            case NOTES:
                affectedRows = database.update(DBHelper.TABLE_NOTES, contentValues, selection, selectionArgs);
                break;

            case NOTES_ID:

                String id = uri.getLastPathSegment();

                if (TextUtils.isEmpty(selection)){
                affectedRows = database.update(DBHelper.TABLE_NOTES, contentValues, DBHelper.NOTE_ID + "=" + id, null);
                }
                else{
                affectedRows = database.update(DBHelper.TABLE_NOTES, contentValues, DBHelper.NOTE_ID + "=" + id + " and " + selection, selectionArgs);
                }
                break;

            default:
                throw new IllegalArgumentException("Unknown URI "+uri);
        }

        getContext().getContentResolver().notifyChange(uri,null);
        return affectedRows;
    }

    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        URI_MATCHER.addURI(AUTHORITY, BASE_PATH_NOTE, NOTES);
        URI_MATCHER.addURI(AUTHORITY, BASE_PATH_NOTE + "/#", NOTES_ID);
    }

}
