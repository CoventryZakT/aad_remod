package com.example.aad_remod;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

public class NoteManager {
    private Context mContext;
    private static NoteManager sNoteManagerInstance = null;

    public static NoteManager newInstance(Context context){
        if (sNoteManagerInstance == null){
            sNoteManagerInstance = new NoteManager(context.getApplicationContext());
        }
        return sNoteManagerInstance;
    }

    private NoteManager(Context context){
        this.mContext = context.getApplicationContext();
    }

    public long create(Notes note){
        ContentValues values = new ContentValues();
        values.put(DBHelper.NOTE_TITLE, note.getTitle());
        values.put(DBHelper.NOTE_CONTENT, note.getContent());
        values.put(DBHelper.NOTE_CREATED_TIME, System.currentTimeMillis());
        values.put(DBHelper.NOTE_MODIFIED_TIME, System.currentTimeMillis());
        Uri result = mContext.getContentResolver().insert(NoteContentProvider.CONTENT_URI, values);
        long id = Long.parseLong(result.getLastPathSegment());
        return id;
    }

    public List<Notes> getAllNotes(){
        List<Notes> notes = new ArrayList<Notes>();
        Cursor cursor = mContext.getContentResolver().query(NoteContentProvider.CONTENT_URI, DBHelper.ALL_COLUMNS, null, null, null);
        if (cursor != null){
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                notes.add(Notes.getNoteFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return notes;
    }

    public Notes getNote(Long id){
        Notes note;
        Cursor cursor = mContext.getContentResolver().query(NoteContentProvider.CONTENT_URI, DBHelper.ALL_COLUMNS, DBHelper.NOTE_ID+" = "+id, null, null);
        if (cursor != null){
            cursor.moveToFirst();
            note = Notes.getNoteFromCursor(cursor);
            return note;
        }
        return null;
    }

    public void update(Notes note){
        ContentValues values = new ContentValues();
        values.put(DBHelper.NOTE_TITLE, note.getTitle());
        values.put(DBHelper.NOTE_CONTENT, note.getContent());
        values.put(DBHelper.NOTE_CREATED_TIME, System.currentTimeMillis());
        values.put(DBHelper.NOTE_MODIFIED_TIME, System.currentTimeMillis());
        mContext.getContentResolver().update(NoteContentProvider.CONTENT_URI, values, DBHelper.NOTE_ID+"="+note.getId(),null);
    }

    public void delete(Notes note){
        mContext.getContentResolver().delete(NoteContentProvider.CONTENT_URI, DBHelper.NOTE_ID + "=" + note.getId(), null);
    }


}
