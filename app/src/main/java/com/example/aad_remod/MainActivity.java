package com.example.aad_remod;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
        import android.net.Uri;
        import android.os.Bundle;
        import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.materialize.util.KeyboardUtil;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private com.mikepenz.materialdrawer.Drawer result = null;

    private Toolbar mToolbar;

    private void openFragment (final Fragment fragment, String title){
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();

        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(mToolbar);

        if (savedInstanceState == null){
            NotePlainEditorFragment fragment = new NotePlainEditorFragment();
            openFragment(fragment, "Note Editor");
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.NOTE_TITLE, "New Note");

        result = new DrawerBuilder().withActivity(this).withToolbar(mToolbar).withActionBarDrawerToggle(true).addDrawerItems(
                new PrimaryDrawerItem().withName("Home").withIcon(FontAwesome.Icon.faw_home).withIdentifier(1),
                new PrimaryDrawerItem().withName("Editor").withIcon(FontAwesome.Icon.faw_edit).withIdentifier(2),
                new PrimaryDrawerItem().withName("Settings").withIcon(FontAwesome.Icon.faw_list).withIdentifier(3)
        )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem != null && drawerItem instanceof Nameable){
                            String name = ((Nameable)drawerItem).getName().getText(MainActivity.this);
                            mToolbar.setTitle(name);
                        }
                        if (drawerItem != null){
                            int selectedScreen = drawerItem.getIdentifier();
                            switch (selectedScreen){
                                case 1:
                                    openFragment(new NoteListFragment(), "Notes");
                                    break;

                                case 2:
                                    startActivity(new Intent(MainActivity.this, NoteEditorActivity.class));
                                    break;

                                case 3:
                                    Toast.makeText(MainActivity.this, "Settings clicked", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }
                        return false;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener(){

                    @Override
                    public void onDrawerOpened(View view){
                        KeyboardUtil.hideKeyboard(MainActivity.this);
                    }

                    @Override
                    public void onDrawerClosed(View view){

                    }

                    @Override
                    public void onDrawerSlide(View view, float v){

                    }
                })
                .withFireOnInitialOnClick(true)
                .withSavedInstance(savedInstanceState)
                .build();
        if (savedInstanceState == null){
            result.setSelection(1);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
