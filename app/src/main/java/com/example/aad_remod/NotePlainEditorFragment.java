package com.example.aad_remod;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class NotePlainEditorFragment extends Fragment {

    private View mRootView;
    private EditText mTitleEditText;
    private EditText mContentEditText;
    private Notes mCurrentNote = null;

    public NotePlainEditorFragment(){

    }

    private void makeToast(String msg){
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    public static NotePlainEditorFragment newInstance (long id){
        NotePlainEditorFragment fragment = new NotePlainEditorFragment();

        if(id>0){
            Bundle bundle = new Bundle();
            bundle.putLong("id", id);
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getCurrentNote();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_note_edit_plain, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_delete:
                if(mCurrentNote != null){
                    promptForDelete();
                }
                else {
                    makeToast("Cannot delete unsaved note");
                }
                break;

            case R.id.action_save:
                if (saveNote()){
                    makeToast("Note saved");
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_note_plain_editor, container, false);
        mTitleEditText = mRootView.findViewById(R.id.edit_text_title);
        mContentEditText = mRootView.findViewById(R.id.edit_text_note);
        return mRootView;
    }


    private void getCurrentNote(){
        Bundle args = getArguments();
        if (args != null && args.containsKey("id")){
            long id = args.getLong("id", 0);
            if (id>0){
                mCurrentNote = NoteManager.newInstance(getActivity()).getNote(id);
            }
        }
    }

    public void onResume(){
        super.onResume();
        if(mCurrentNote != null){
            populateFields();
        }
    }

    private boolean saveNote(){
        String title = mTitleEditText.getText().toString();
        if(TextUtils.isEmpty(title)){
            mTitleEditText.setError("Title is Required");
            return false;
        }

        String content = mContentEditText.getText().toString();
        if (TextUtils.isEmpty(content)){
            mContentEditText.setError("Content is required");
            return false;
        }

        if(mCurrentNote != null){
            mCurrentNote.setTitle(title);
            mCurrentNote.setContent(content);
            NoteManager.newInstance(getActivity()).update(mCurrentNote);
        }
        else {
            Notes note = new Notes();
            note.setTitle(title);
            note.setContent(content);
            NoteManager.newInstance(getActivity()).create(note);
        }
        return true;
    }

    public void promptForDelete(){
        final String titleOfTBDNote = mCurrentNote.getTitle();
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Delete \"" + titleOfTBDNote + "\"?");
        alertDialog.setMessage("Are you sure you wish to delete the note \""+titleOfTBDNote+"\"?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                NoteManager.newInstance(getActivity()).delete(mCurrentNote);
                makeToast(titleOfTBDNote+" deleted");
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }

    private void populateFields(){
        mTitleEditText.setText(mCurrentNote.getTitle());
        mContentEditText.setText(mCurrentNote.getContent());
    }
}
