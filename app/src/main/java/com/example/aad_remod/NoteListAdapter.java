package com.example.aad_remod;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.ViewHolder> {

    private List<Notes> mNotes;
    private Context mContext;


    public NoteListAdapter(List<Notes> notes, Context context){
        mNotes = notes;
        mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_note_list, parent, false);
        ViewHolder v = new ViewHolder(rowView);
        return v;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int pos){
        holder.noteTitle.setText(mNotes.get(pos).getTitle());
        holder.noteCreateDate.setText(mNotes.get(pos).getTitle());
    }

    @Override
    public int getItemCount(){
        return mNotes.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        public final TextView noteTitle;
        public final TextView noteCreateDate;

        public ViewHolder(View itemView){
            super(itemView);
            noteTitle = itemView.findViewById(R.id.text_note_title);
            noteCreateDate = itemView.findViewById(R.id.text_note_date);

        }

    }

}
