package com.example.aad_remod;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.SyncStateContract;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "notes.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_NOTES = "notes";
    public static final String NOTE_ID = "_id";
    public static final String NOTE_TITLE = "title";
    public static final String NOTE_CONTENT = "content";
    public static final String NOTE_CREATED_TIME = "created_time";
    public static final String NOTE_MODIFIED_TIME = "modified_time";
    public static final String NOTE_DUE_TIME = "due_time";

    public static final String[] ALL_COLUMNS = {
            NOTE_ID,
            NOTE_TITLE,
            NOTE_CONTENT,
            NOTE_CREATED_TIME,
            NOTE_MODIFIED_TIME,
            //NOTE_DUE_TIME
            };

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_NOTE);
    }



    @Override
    public void onUpgrade (SQLiteDatabase sqLiteDatabase,int oldVer, int newVer){
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);
    }

    private static final String CREATE_TABLE_NOTE =
            "CREATE TABLE NOTES"+
                    "("+
                    NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+
                    NOTE_TITLE + " TEXT NOT NULL,"+
                    NOTE_CONTENT + " TEXT,"+
                    NOTE_CREATED_TIME + " INTEGER NOT NULL"+","+
                    NOTE_MODIFIED_TIME + " INTEGER NOT NULL"+//","+
                    //NOTE_DUE_TIME + " INTEGER"+
                    ")";


}
