package com.example.aad_remod;

import android.database.Cursor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Notes {
    private Long id;
    private String title;
    private String content;
    private long dateCreated;
    private long dateModified;

    public static Notes getNoteFromCursor(Cursor cursor){
        Notes note = new Notes();
        note.setId(cursor.getLong(cursor.getColumnIndex(DBHelper.NOTE_ID)));
        note.setTitle(cursor.getString(cursor.getColumnIndex(DBHelper.NOTE_TITLE)));
        note.setContent(cursor.getString(cursor.getColumnIndex(DBHelper.NOTE_CONTENT)));
        note.setDateCreated(cursor.getLong(cursor.getColumnIndex(DBHelper.NOTE_CREATED_TIME)));
        note.setDateModified(cursor.getLong(cursor.getColumnIndex(DBHelper.NOTE_MODIFIED_TIME)));
        return note;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getDateModified() {
        return dateModified;
    }

    public void setDateModified(long dateModified) {
        this.dateModified = dateModified;
    }

    public String getReadableModifiedDate(){
        Date d = new Date(getDateModified()*1000);
        String modifiedDate = d.toString();
        return modifiedDate;
    }
}
